<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Task extends Model
{
    //
    protected $table = 'tasks';
    //agar yang dilakukan soft delete
    use SoftDeletes;

    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
      }
}
