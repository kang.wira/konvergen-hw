<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public function index()
    {
        $users = User::orderBy('id', 'asc')->get();
        return view('user',['user' => $users]);
    }

    public function view_create()
    {
        return view('user-create');
    }

    public function view_update($user_id)
    {
        $user = User::find($user_id);
        return view('user-update',['user' => $user]);
    }

    public function create(Request $request)
    {
        
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->role = 'user';
        $user->save();
        return redirect()->action('HomeController@index');
    }

    public function update(Request $request)
    {
       
        $user = User::find($request->id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if ($request->input('password')!=''){
            $user->password = Hash::make($request->input('password'));
        }
        
        $user->save();
        return redirect()->action('HomeController@index');
    }

    public function delete($user_id)
    {
        $user = User::find($user_id);
        $user->delete();
        return redirect()->action('HomeController@index');
    }
}
