<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use Auth;
use Storage;


class TaskController extends Controller
{
    //
    public function index()
    {
        if(Auth::user()->role == 'admin'){
            $tasks = Task::with('user')->orderBy('created_at', 'desc')->get();
        }else{
            $tasks = Task::with('user')->orderBy('created_at', 'desc')->get();
        }
        // dd($tasks);
        return view('task',['tasks' => $tasks]);
    }

    public function get_assigned()
    {
        $tasks = Task::where('user_id','=',Auth::user()->id)->orderBy('created_at', 'desc')->get();
        return view('task-assigned',['tasks' => $tasks]);
    }

    public function delete($task_id)
    {
        $task = Task::find($task_id);
        $task->delete();
        return redirect()->action('TaskController@index');
    }

    public function book($task_id)
    {
        $task = Task::find($task_id);
        $task->user_id = Auth::user()->id;
        $task->save();
        return redirect()->action('TaskController@index');
    }

    public function revoke($task_id)
    {
        $task = Task::find($task_id);
        //buat user_id di task menjadi 0
        $task->user_id=0;
        $task->save();
        return redirect()->action('TaskController@index');
    }

    public function view_upload($task_id)
    {
        $task = Task::find($task_id);
        return view('task-upload',['task' => $task]);
    }

    public function upload(Request $request)
    {
        $task = Task::find($request->id);
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        //hanya save jika file zip rar ato 7z (kurang tau file zip lain apa)
        if ($ext == 'zip' || $ext == 'rar' || $ext == '7z'){
            $name = $file->getClientOriginalName();
            $path = $file->storeAs('public/uploads', $name);
            //save nama file ke db task ybs
            $task->file=$name;
            $task->save();
            return redirect()->action('TaskController@index');
        }else{
            return redirect()->back()->withErrors(['Format file harus zip, rar atau 7z']);
        }
        
    }

    public function download($task_id)
    {
        $task = Task::find($task_id);
        //prepare lokasi download file given dari nama file di db 
        $download = public_path().'/storage/uploads/'.$task->file;
        return response()->download($download);
    }

    public function view_create()
    {
        return view('task-create');
    }

    public function create(Request $request)
    {
        
        $task = new Task;
        $task->name = $request->input('name');
        $task->save();
        return redirect()->action('TaskController@index');
    }

}
