<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CheckCodeStandart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:code_standart {--fix : Fix automaticaly with phpcbf} {files?*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Code standart';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($fix = $this->option('fix')) {
            $this->info('Running PHP Code Beautifier Fix......');
        } else {
            $this->info('Running PHP Code Sniffer......');
        }

        $this->info('Including configuration file.');
        $files = $this->argument('files');
        $executeable = $fix ? config('codestandart.phpcbf_executable') : config('codestandart.phpcs_executable');
        $ignoreArray = config('codestandart.ignore');
        $ignore = implode(',', $ignoreArray);
        $standard = config('codestandart.standard');

        /**
         * construct command
         */
        $command = $executeable.' --standard='.$standard.' --ignore='.$ignore;

        /**
         * remove from list ignored file
         */
        if (count($files) !== 0) {
            $this->info('Remove ignored file from list.');
            foreach ($files as $index => $file) {
                if (Str::contains($file, $this->removeAsteriks($ignoreArray))) {
                    unset($files[$index]);
                }
            }

            $command .= ' '.implode(' ', $files);
        } else {
            $root_project = base_path();
            $command .= ' --cache '.$root_project;
        }

        $this->info('Executing command....');
        $this->info($command);
        exec($command, $results);


        foreach ($results as $resultLine) {
            $this->error($resultLine);
        }

        if (count($results) == 0) {
            $this->info('Hell Yeah... Your code is cool...');
        } else {
            exit(1);
        }
    }

    public function removeAsteriks($ignore)
    {
        return preg_replace("/[^a-zA-Z 0-9 \/ \- \_]+/", "", $ignore);
    }
}
