<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //untuk sementara task dipopulate dari seeded
        DB::table('tasks')->insert([
            ['name' => 'task 1'],
            ['name' => 'task 2'],
            ['name' => 'task 3'],
            ['name' => 'task 4'],
            ['name' => 'task 5'],
            ['name' => 'task 6'],
            ['name' => 'task 7'],
            ['name' => 'task 8'],
            ['name' => 'task 9'],
            ['name' => 'task 10'],
            ['name' => 'task 11'],
            ['name' => 'task 12'],
            ['name' => 'task 13'],
            ['name' => 'task 14'],
            ['name' => 'task 15']
        ]);
    }
}
