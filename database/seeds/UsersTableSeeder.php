<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insertGetId([
          'name' => 'Administrator',
          'email' => 'a@a.com',
          'password' => bcrypt('123456'),
          'role' => 'admin',
          'email_verified_at' => now(),
          'created_at' => now(),
          'updated_at' => now(),
        ]);
    }
}
