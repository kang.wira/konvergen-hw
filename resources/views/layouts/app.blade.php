<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>@yield('title') - {{ env('APP_NAME') }}</title>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('css/helper.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    @stack('css')

    <!-- Fontawesome -->
    <script src="https://kit.fontawesome.com/48f1571049.js"></script>
  </head>
  <body class="bg-main-gray-100 h-screen antialiased">
    @include('layouts.header')
    
    <div class="flex h-content-screen">
      @include('layouts.navigation')
      @yield('content')
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    @stack('scripts')
  </body>
</html>