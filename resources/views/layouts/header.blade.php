@php
  $temp = explode(" ", Auth::user()->name);
  
  $admin_name = '';

  for ($i=0; $i < count($temp); $i++) { 
    # code...
    if($i == 0) {
      $admin_name = $temp[$i][0];
    } else {
      $admin_name .= $temp[$i][0];
    }
  }
@endphp

<nav class="flex items-center justify-between flex-wrap bg-white shadow-bottom">
  <!-- Logo -->
  <div class="flex items-center bg-main-blue shadow-right text-white px-6 w-auto lg:w-56 h-16">
    <img class="h-12" src="{{ asset('img/logo.png') }}" alt="Logo">
    <div class="ml-4 hidden lg:block">
      <p class="font-semibold text-lg uppercase">Admin</p>
      <p class="font-semibold text-lg uppercase">D-Laundry</p>
    </div>
  </div><!-- End of logo -->

  <!-- Menu -->
  <div class="flex items-center px-6">
    <div class="text-sm mr-3 lg:mr-0">
<!--       <a href="#responsive-header" class="rounded-full h-6 w-6 lg:h-8 lg:w-8 inline-flex items-center justify-center mr-3 lg:mr-5 bg-main-blue inline-block">
        <i class="fas fa-bell text-white"></i>
      </a> -->
      <span class="mr-3 lg:mr-5 inline-block">
        <!-- <span href="#responsive-header" class="rounded-full h-6 w-6 lg:h-8 lg:w-8 inline-flex items-center justify-center mr-1 bg-main-blue">
          <i class="fas fa-user text-white"></i>
        </span> -->
        <span class="text-main-gray font-semibold hidden lg:inline-flex">{{ Auth::user()->name }}</span>
        
      </span>
      <form action="{{ route('logout')}}" method="post" class="inline-block">
        @csrf
        <button type="submit" class="rounded-full h-6 w-6 lg:h-8 lg:w-8 inline-flex items-center justify-center bg-main-blue">
          <i class="fas fa-sign-out-alt text-white"></i>
        </button>
      </form>
    </div>
    <div class="block lg:hidden">
      <button class="flex items-center px-3 py-2 border rounded text-white border-white">
        <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
      </button>
    </div>
  </div>
  <!-- End of menu -->
</nav>