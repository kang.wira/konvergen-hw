@php
  $active = false;
  $menus = config('menus.menu');
  @endphp
 


  <nav class="flex-none bg-main-blue shadow-right pr-3 w-56 z-10 hidden lg:block">
  <ul class="list-none py-16 text-white text-sm">
  @foreach ($menus as $menu)   
    <li id="{{$menu['name']}}" class="capitalize mb-3">
      <a class="flex items-center py-3 px-6 font-semibold  hover:text-main-blue hover:font-semibold hover:bg-main-gray-100 hover:rounded-r-lg @if((request()->segment(1) === $menu['name'])) active bg-main-gray-100 rounded-r-lg text-main-blue @endif" href="{{ url($menu['url']) }}">
          <img class="w-6 mr-5 svg-nav" src="{{ asset($menu['icon']) }}" alt="Order">
          {{ $menu['title'] }}
        </a>
      </li>
  @endforeach
  </ul>
</nav>
