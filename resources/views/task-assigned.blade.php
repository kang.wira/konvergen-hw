@extends('adminlte::page')

@section('title', 'Task Assigned')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Dashboard</div>

            <div class="card-body">
            <div class="panel panel-default">
            <div class="panel-heading">
                Task Assigned
            </div>

            <div class="panel-body">
                <table class="table table-striped task-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>File</th>
                      
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($tasks as $task)
                            <tr>
                                <!-- Table Content -->
                                <td class="table-text">
                                    <div>{{ $task->id }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $task->name }}</div>
                                </td>
                                <td class="table-text">
                                    @if(!is_null($task->file))
                                    <a href="{{url('/task/file/'.$task->id)}}"  class="btn btn-sm btn-warning">Download File</a>
                                    @endif
                                    @if(is_null($task->file))
                                    <div>Kosong</div>
                                    @endif
                                </td>
                               
                               
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
            </div>
        </div>
    </div>
</div>
@stop