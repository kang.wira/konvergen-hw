@extends('adminlte::page')

@section('title', 'Task List')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Dashboard</div>

            <div class="card-body">
            <div class="panel panel-default">
            <div class="panel-heading">
                Task List
                <a href="{{url('/task/view-create')}}"  class="btn btn-sm btn-warning">Create Task</a>
            </div>

            <div class="panel-body">
                <table class="table table-striped task-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Penanggung Jawab</th>
                        <th>Action</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($tasks as $task)
                            <tr>
                                <!-- Table Content -->
                                <td class="table-text">
                                    <div>{{ $task->id }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $task->name }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ !empty($task->user) ? $task->user->name:''}}</div>
                                </td>
                               
                                <td>
                                    @if($task->user_id == 0)
                                    <a href="{{url('/task/book/'.$task->id)}}"  class="btn btn-sm btn-warning">Book Task</a>
                                    @endif
                                    @if(Auth::user()->role == 'admin')
                                    <a href="{{url('/task/revoke/'.$task->id)}}"  class="btn btn-sm btn-warning">Revoke Task</a>
                                    <a href="{{url('/task/delete/'.$task->id)}}"  class="btn btn-sm btn-warning">Delete Task</a>
                                    <a href="{{url('/task/view-upload/'.$task->id)}}"  class="btn btn-sm btn-warning">Upload File</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
            </div>
        </div>
    </div>
</div>
@stop