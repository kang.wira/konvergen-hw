@extends('adminlte::page')

@section('title', 'Update User')
@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
      

        <!-- The Form -->
        <form action="{{ url('user/update')}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden"  name="id" id="id" value="{{ $user->id }}" />
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label">Name</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}">
                </div>
            </div>
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label">Email</label>

                <div class="col-sm-6">
                    <input type="text" name="email" id="email" class="form-control" value="{{ $user->email }}">
                </div>
            </div>
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label">Password</label>

                <div class="col-sm-6">
                    <input type="password" name="password" id="password" class="form-control">
                </div>
            </div>
           
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Update User
                    </button>
                </div>
            </div>
        </form>
    </div>

    <!-- TODO: Current Tasks -->
@endsection