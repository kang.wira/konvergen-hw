@extends('adminlte::page')

@section('title', 'Upload Task File')

@section('content')
<div class="panel-body">
        <!-- Display Validation Errors -->
        @if($errors->any())
            <h4>{{$errors->first()}}</h4>
        @endif

        <!-- The Form -->
        <form action="{{ url('task/upload')}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden"  name="id" id="id" value="{{ $task->id }}" />
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label">File</label>

                <div class="col-sm-6">
                    <input type="file" name="file" id="file" class="form-control">
                </div>
            </div>
           
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="upload" class="btn btn-default">
                        <i class="fa fa-plus"></i> Upload File
                    </button>
                </div>
            </div>
        </form>
    </div>

@endsection