@extends('adminlte::page')

@section('title', 'Home')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Dashboard</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                You are logged in!


                @if(Auth::user()->role == 'admin')
                
                <div class="panel panel-default">
                <div class="panel-heading">
                    User List
                    <a href="{{url('/user/view-create')}}"  class="btn btn-sm btn-warning">Create User</a>
                </div>

                <div class="panel-body">
                
                    <table class="table table-striped task-table">

                        <!-- Table Headings -->
                        <thead>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Action</th>
                        </thead>

                        <!-- Table Body -->
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <!-- Table Content -->
                                    <td class="table-text">
                                        <div>{{ $user->id }}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{ $user->name }}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{ $user->email}}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{ $user->role}}</div>
                                    </td>
                                    <td>
                                        <a href="{{url('/user/view-update/'.$user->id)}}"  class="btn btn-sm btn-warning">Update</a>
                                        @if($user->role != 'admin')
                                        <a href="{{url('/user/delete/'.$user->id)}}"  class="btn btn-sm btn-warning">Delete</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@stop