<?php

return [
  /*
    |--------------------------------------------------------------------------
    | php code sniffer executable
    |--------------------------------------------------------------------------
    |
    | this value to define php code sniffer executable
    |
    */
   'phpcs_executable' => base_path('vendor/bin/phpcs'),
   'phpcbf_executable' => base_path('vendor/bin/phpcbf'),
   /*
    |--------------------------------------------------------------------------
    | code standart
    |--------------------------------------------------------------------------
    |
    | this value to define code standart
    |
    */
   'standard' => 'PSR2',
  /*
    |--------------------------------------------------------------------------
    | ignore folder
    |--------------------------------------------------------------------------
    |
    | this value to ignore code sniffer for several folder
    |
    */
  'ignore' => [
    public_path('index.php'),
    base_path('vendor/*'),
    base_path('bootstrap/*'),
    base_path('storage/*'),
    base_path('database/migrations/*'),
    base_path('database/seeds/*'),
    public_path('*'),
    resource_path('views/*'),
  ],
];
