<?php


  return[
    'admin_menu' => [
        'MAIN NAVIGATION',
        [
            'text' => 'Task',
            'url'  => '/task',
            // 'can'  => 'manage-blog',
        ],
        [
            'text'        => 'Task Assigned',
            'url'         => '/task/assigned',
        ],
        [
            'text'        => 'Users',
            'url'         => '/user',
        ],
        
    ],
    'menu' => [
        'MAIN NAVIGATION',
        [
            'text' => 'Task',
            'url'  => '/task',
            // 'can'  => 'manage-blog',
        ],
        [
            'text'        => 'Task Assigned',
            'url'         => '/task/assigned',
        ],
        
    ],
];