<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Helper
    |--------------------------------------------------------------------------
    |
    | Helper config
    |
    */

    'user' => [
        'picture' => 'https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg',
        'name' => 'Akmal Reza',
    ],
];
