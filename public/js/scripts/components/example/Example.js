define(['base'], (Base) => {
    return class Example extends Base {
        constructor(identifier, dispatcher) {
          super({identifier, dispatcher});
        }

        componentDidMount() {
            this.state = {
                loading: true,
                identifier: '',
                profile: {
                    name: 'Arief Hikam',
                    age: 18,
                }
            }

            this.setState(['identifier'], this.props.identifier);
        }

        render() {
            return this.view('components/example/template', this.state);
        }

        events() {
            return {
                'REFRESH': () => this.refresh(), 
            }
        }
        
        delay(delayInms) {
          return new Promise(resolve  => {
            setTimeout(() => {
              resolve(2);
            }, delayInms);
          });
        }

        async refresh() {
            this.loading(true);
            this.setState(['profile', 'name'], Math.random().toString(36).substring(7));
            await this.delay(500);
            this.reRender(); 
        }

    }
});