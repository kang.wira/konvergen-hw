require.config({
    baseUrl: "/js/scripts",
});

function load(modules) {
    return new Promise((resolve, reject) => {
        requirejs(modules, (template) => {
            resolve(template);
        });
    })
}

async function useModules() {
    dispatcher = await load(['dispatcher']);
    components = {
        Example: await load(['components/example/Example']),
    }

    globalDispatcher = new dispatcher;

    return {
        GlobalDispatcher: globalDispatcher,
        Dispatch: (type, data) => {
            globalDispatcher.dispatch(type, data);
        },
        ...components,
    }
}