define(() => {
    return class Base {
        constructor(props = false) {
            this.props = props;
            this.onDOMReady();
        }

        /**
         * run after component mounted and ready
         */
        componentDidMount() {}

        onDOMReady() {
            this.componentDidMount();
            this.reRender();
            this.registerEvents(this.props.dispatcher);
        }

        /**
         * set state
         * @param {string}  path           
         * @param {any}  value          
         * @param {Boolean} setrecursively 
         */
        setState(path, value, setrecursively = false){
            let level = 0;

            path.reduce((a, b)=>{
                level++;
                if (setrecursively && typeof a[b] === "undefined" && level !== path.length){
                    a[b] = {};
                    return a[b];
                }

                if (level === path.length){
                    a[b] = value;
                    return value;
                } else {
                    return a[b];
                }
            }, this.state);
        }

        /**
         * loading html
         */
        loadingHtml() {
            return this.view('components/loading/template');
        }

        /**
         * parse template to code generation dynamicly
         * @param  {string} template html string
         * @param  {Object} data     data pass object
         * @return {string}          code generated string
         */
        parseTemplate(template, data) {
            const names = Object.keys(data);
            const vals = Object.values(data);
            return new Function(...names, `return \`${template}\`;`)(...vals);
        }

        /**
         * render view
         * @return {string} string code generated string
         */
        render() {
            return '';
        }

        /**
         * re render to DOM
         */
        reRender() {
            this.render().then((value) => {
                this.renderToHTML(value);
            })
        }

        /**
         * get view from string template and parse
         * @param  {string} location location string
         * @param  {Object} data     data object
         * @return {string}          generated code string
         */
        async view(location, data = {}) {
            return await this.loadAsync(location, data);
        }

        loadAsync(location, data = {}) {
            return new Promise((resolve, reject) => {
                requirejs([`text!${location}.view`], (template) => {
                    resolve(this.parseTemplate(template, data));
                });
            })
        }

        /**
         * set to HTML DOM
         * @param  {string} html string html
         */
        renderToHTML(html) {
            let identifier = this.state.identifier;
            document.getElementById(identifier).innerHTML = html;
        }

        /**
         * render html to loading screen
         * @param  {Boolean} loading [description]
         * @return {[type]}          [description]
         */
        loading(loading = true){
            let identifier = this.state.loadingIdentifier ? this.state.loadingIdentifier : this.state.identifier;
            if (loading) {
                this.loadingHtml().then((value) => {
                    this.renderToHTML(value);
                });
            } else {
                this.renderToHTML('');
            }
            this.setState(['loading'], loading);
        }

        registerEvents(dispatcher) {
            const events = this.events();
            for (const key of Object.keys(events)) {
                dispatcher.add(key, events[key]);
            }
        }
    }
})