

## How to install
- copy env from example
> cp .env.example .env

- give full access to storage folder
> chmod -R 777 storage
- install default package with composer 
> composer install
- install default frontend package with NPM
> npm install
- compile frontend
> npm run dev
- linking storage file
> php artisan storage:link
## migration and seeding
- touch storage/app/db.sqlite
- composer dump-autoload
- php artisan migrate
- php artisan db:seed
## cara menjalankan
- php artisan serve
- saat seed sudah diberikan 1 admin email a@a.com password 123456