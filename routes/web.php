<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect('/home');
});
//task
Route::get('/task', ['uses' => 'TaskController@index', 'as' => 'task.list']);
Route::get('/task/assigned', ['uses' => 'TaskController@get_assigned', 'as' => 'task.assigned']);
Route::get('/task/book/{id}', ['uses' => 'TaskController@book', 'as' => 'task.delete']);
Route::get('/task/delete/{id}', ['uses' => 'TaskController@delete', 'as' => 'task.delete']);
Route::get('/task/revoke/{id}', ['uses' => 'TaskController@revoke', 'as' => 'task.revoke']);
Route::get('/task/view-upload/{id}', ['uses' => 'TaskController@view_upload', 'as' => 'task.view_upload']);
Route::post('/task/upload', ['uses' => 'TaskController@upload', 'as' => 'task.upload']);
Route::get('/task/file/{id}', ['uses' => 'TaskController@download', 'as' => 'task.download']);
Route::get('/task/view-create', ['uses' => 'TaskController@view_create', 'as' => 'task.view_create']);
Route::post('/task/create', ['uses' => 'TaskController@create', 'as' => 'task.create']);


//user
Route::get('/user', ['uses' => 'UserController@index', 'as' => 'user.list']);
Route::get('/user/view-create', ['uses' => 'UserController@view_create', 'as' => 'user.view_create']);
Route::post('/user/create', ['uses' => 'UserController@create', 'as' => 'user.create']);
Route::get('/user/view-update/{id}', ['uses' => 'UserController@view_update', 'as' => 'user.view_update']);
Route::post('/user/update', ['uses' => 'UserController@update', 'as' => 'user.update']);
Route::get('/user/delete/{id}', ['uses' => 'UserController@delete', 'as' => 'user.delete']);

Route::get('/home', 'HomeController@index')->name('home');
